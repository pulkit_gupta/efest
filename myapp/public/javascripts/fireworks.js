var Fireworks = (function ()
{
    // declare the variables we need
    var particles = [],
        mainCanvas = null,
        mainContext = null,
        fireworkCanvas = null,
        fireworkContext = null,
        viewportWidth = 0,
        viewportHeight = 0;
        hold = 0, // hold speed
        speedmodes = 0;
        finish = 1,
        clear_on = 1, // clear context
        animation_on = 0; // save CPU
        var crackerType = 0,
        crackerpoint = {
            x: 0,
            y: 0
        }, mouseactive = 0,
        holdRocket = 0,
        circle = 0, //change circle
        multi = 0,
        fireSound = "fire1",
        stoppage = 0,
        stopset = 0;
        var imageposition = 0;
        var spark = {
        x: 0,
        y: 0
        };
        var anarVel = 0,
        stopanar = 0,
        anarType = 0;
        //green 336 ,924 purple yellow 72888 neela 192 yellowpale yello 264 pink 1140dark green 432 s588 skyblue132 golden colour 948 purple
        var anarPattern = 0,
        colorIndex = 0;
        var anar_color_array = new Array(336, 924, 72, 888, 192, 264, 1140, 432, 588, 132); //0 1o 9
        var base_image = new Image(),
        stick_image = new Image(),
        clear_stick = new Image();
        stick_image.src = "images/crackers/stick.png";
        clear_stick.src = "images/crackers/stick_erase.png";
        var burn_image = new Image();
        var randIndex = 0,
        rand = [];
        var dragging = false,
        imageOffset = {
            x: 0,
            y: 0
        },
        mousePosition;

    function randfunc()
    {
        randIndex++;
        if (randIndex == 350)
        {
            randIndex = 0;
        }
        //console.log("value at"+randIndex+"is"+rand[randIndex]);
        //return Math.random();
        return rand[randIndex];
    }
    /////////////////////////////////Mouse Code////////////////////////////////////////////
    function dragImage(coords)
    {
        mainContext.drawImage(clear_stick, imageOffset.x, imageOffset.y);
        //console.log("mouse moved and corodinates are"+coords.x+","+coords.y);
        //console.log("value of startinmg of x"+imageOffset.x);
        imageOffset.x -= mousePosition.x - coords.x;
        imageOffset.y -= mousePosition.y - coords.y;
        //console.log("value of now of x"+imageOffset.x);
        //imageOffset.x = mousePosition.x-150;
        //imageOffset.y = mousePosition.y-10;
        // constrain coordinates to keep the image visible in the canvas
        //imageOffset.x = Math.min(0, Math.max(imageOffset.x, viewportWidth));
        //imageOffset.y = Math.min(0, Math.max(imageOffset.y, viewportHeight));
        mousePosition = coords;
        drawImage();
    }

    function drawImage()
    {
        mainContext.drawImage(base_image, imageposition.x, imageposition.y);
        mainContext.drawImage(stick_image, imageOffset.x, imageOffset.y);
        if (Math.abs(crackerpoint.x - imageOffset.x) < 20 && Math.abs(crackerpoint.y - imageOffset.y) < 20)
        {
            mouseactive = 0;
            dragging = false;
            mainContext.drawImage(clear_stick, imageOffset.x, imageOffset.y);
            mainContext.drawImage(base_image, imageposition.x, imageposition.y);
            launch();
        }
        // draw at the position recorded in imageOffset
        // don't forget to clear the canvas before drawing
    }

    function getMouseCoords(e)
    {
        // return the position of the mouse relative to the top left of the canvas
        return {
            x: e.offsetX,
            y: e.offsetY
        }
    }
    ////////////////////////////////////////////////////////////////////////////////
    function initialize()
    {
        //holdRocket=0, multi=0, circle=0, crackerType=1, imgname='crackers/rocket1.png';
        soundManager.setup(
        {
            url: 'swf/',
            onready: function ()
            {
                soundManager.createSound(
                {
                    id: 'fire1',
                    url: 'skyrocket1.wav'
                });
                soundManager.createSound(
                {
                    id: 'fire2',
                    url: 'skyrocket2.mp3'
                });
                soundManager.createSound(
                {
                    id: 'fire3',
                    url: 'skyrocket3.mp3'
                });
                soundManager.createSound(
                {
                    id: 'blast',
                    url: 'explode.wav'
                });
                soundManager.createSound(
                {
                    id: 'anar1',
                    url: 'anar.mp3'
                });
                soundManager.createSound(
                {
                    id: 'anarlong',
                    url: 'anarlong.mp3'
                });
            }
        });
        // start by measuring the viewport
        onWindowResize();
        // create a canvas for the fireworks
        mainCanvas = document.createElement('canvas');
        mainContext = mainCanvas.getContext('2d');
        // and another one for, like, an off screen buffer
        // because that's rad n all
        fireworkCanvas = document.createElement('canvas');
        fireworkContext = fireworkCanvas.getContext('2d');
        // set up the colours for the fireworks
        createFireworkPalette(12);
        // set the dimensions on the canvas
        setMainCanvasDimensions();
        // add the canvas in
        mainCanvas.onmousedown = function (e)
        {
            if (mouseactive == 1)
            {
                var x1 = getMouseCoords(e).x,
                    y1 = getMouseCoords(e).y;
                if (Math.abs(mousePosition.x - x1) < 130 && Math.abs(mousePosition.y - y1) < 70)
                {
                    //mousePosition={x: x1,y: y1};
                    socket.emit(user.gameid,
                    {
                        event: "pickStick"
                    });
                    dragging = true;
                }
                mainContext.drawImage(clear_stick, imageposition.x + 300, imageposition.y - 350);
                return false;
            }
        };
        document.body.onmouseup = function ()
        {
            if (mouseactive == 1)
            {
                mainCanvas.style.cursor = "url('images/crackers/cursor_hand.png'),auto";
                dragging = false;
            }
        };
        mainCanvas.onmousemove = function (e)
        {
            if (dragging && mouseactive == 1)
            {
                mainCanvas.style.cursor = "url('images/crackers/drag.png'),auto";
                dragImage(getMouseCoords(e));
            }
        };
        document.body.appendChild(mainCanvas);
        var flame = new Image();
        //mainCanvas.addEventListener('mouseup', launch, true); 
        //mainCanvas.addEventListener('mouseup', launch, true); 
        //mainCanvas.addEventListener('touchend', launch, true);
        socket.on(user.gameid, function (data)
        {
            var devent = data.event;            
            if (devent == "setcracker" && finish == 1)
            {
                speedmodes=0;
                randIndex = 0;
                rand = data.randval;
                mainContext.fillStyle = "rgba(0,0,0,1)"; //clearing old images of rocket
                mainContext.fillRect(0, 0, viewportWidth, viewportHeight);
                var type = data.type;
                var imgname = "";
                if (type == 1)
                {
                    holdRocket = 0, multi = 0, circle = 1, crackerType = type;
                    imgname = 'images/crackers/rocket1.png';
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 160
                    };
                    crackerpoint = {
                        x: imageposition.x - 10,
                        y: imageposition.y
                    };
                    spark = {
                        x: viewportWidth / 2 - 2,
                        y: viewportHeight - 125
                    };
                    fireSound = "fire1";
                }
                else if (type == 2)
                {
                    holdRocket = 0, multi = 0, circle = 0;
                    crackerType = type;
                    imgname = 'images/crackers/rocket1.png';
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 160
                    };
                    crackerpoint = {
                        x: imageposition.x - 10,
                        y: imageposition.y
                    };
                    spark = {
                        x: viewportWidth / 2 - 2,
                        y: viewportHeight - 125
                    };
                    fireSound = "fire1";
                }
                else if (type == 3)
                {
                    holdRocket = 0, multi = 1, circle = 1;
                    crackerType = type;
                    imgname = 'images/crackers/rocket3.png';

                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 160
                    };
                    crackerpoint = {
                        x: imageposition.x - 10,
                        y: imageposition.y + 50
                    };
                    spark = {
                        x: viewportWidth / 2 - 2,
                        y: viewportHeight - 75
                    };
                    fireSound = "fire2";
                }
                else if (type == 4)
                {
                    holdRocket = 0, multi = 1, circle = 0;
                    crackerType = type;
                    imgname = 'images/crackers/rocket3.png';
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 160
                    };
                    crackerpoint = {
                        x: imageposition.x - 10,
                        y: imageposition.y + 50
                    };
                    spark = {
                        x: viewportWidth / 2 - 2,
                        y: viewportHeight - 75
                    };
                    fireSound = "fire2";
                }
                else if (type == 5)
                {
                    holdRocket = 1, multi = 0, circle = 1;
                    crackerType = type;
                    imgname = 'images/crackers/rocket2.png';
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 120
                    };
                    crackerpoint = {
                        x: imageposition.x - 20,
                        y: imageposition.y
                    };
                    spark = {
                        x: viewportWidth / 2 - 2,
                        y: viewportHeight - 75
                    };
                    fireSound = "fire3";
                }
                else if (type == 6)
                {
                    holdRocket = 1, multi = 0, circle = 0;
                    crackerType = type;
                    imgname = 'images/crackers/rocket2.png';
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 120
                    };
                    crackerpoint = {
                        x: imageposition.x - 20,
                        y: imageposition.y
                    };
                    spark = {
                        x: viewportWidth / 2 - 2,
                        y: viewportHeight - 75
                    };
                    fireSound = "fire3";
                }
                //WHISTLE
                else if (type == 7)
                {
                    holdRocket = 0, multi = 2, circle = 1;
                    crackerType = type;
                    imgname = 'images/crackers/rocket3.png';
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 160
                    };
                    crackerpoint = {
                        x: imageposition.x - 10,
                        y: imageposition.y + 50
                    };
                    spark = {
                        x: viewportWidth / 2 - 2,
                        y: viewportHeight - 75
                    };
                    fireSound = "fire2";
                    stoppage = 0;
                    stopset = 50;
                }

                else if (type == 8)
                {
                    holdRocket = 0, multi = 2, circle = 0;
                    crackerType = type;
                    imgname = 'images/crackers/rocket3.png';
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 160
                    };
                    crackerpoint = {
                        x: imageposition.x - 10,
                        y: imageposition.y + 50
                    };
                    spark = {
                        x: viewportWidth / 2 - 2,
                        y: viewportHeight - 75
                    };
                    fireSound = "fire2";
                    stoppage = 0;
                    stopset = 50;
                }
                else if (type == 9)
                {
                    holdRocket = 0, multi = 3, circle = 0;
                    crackerType = type;
                    imgname = 'images/crackers/rocket3.png';
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 160
                    };
                    crackerpoint = {
                        x: imageposition.x - 10,
                        y: imageposition.y + 50
                    };
                    spark = {
                        x: viewportWidth / 2 - 2,
                        y: viewportHeight - 75
                    };
                    fireSound = "fire1";
                    stoppage = 0;
                    stopset = 20;
                }
                else if (type == 10)
                {
                    holdRocket = 0, multi = 3, circle = 0;
                    crackerType = type;
                    imgname = 'images/crackers/rocket3.png';
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 160
                    };
                    crackerpoint = {
                        x: imageposition.x - 10,
                        y: imageposition.y + 50
                    };
                    spark = {
                        x: viewportWidth / 2 - 2,
                        y: viewportHeight - 75
                    };
                    fireSound = "fire1";
                    stoppage = 0;
                    stopset = 100;
                }

                else if (type == 11)
                {
                    holdRocket = 0, multi = 0, circle = 1;
                    speedmodes=1;
                    crackerType = type;
                    imgname = 'images/crackers/rocket3.png';
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 160
                    };
                    crackerpoint = {
                        x: imageposition.x - 10,
                        y: imageposition.y + 50
                    };
                    spark = {
                        x: viewportWidth / 2 - 2,
                        y: viewportHeight - 75
                    };
                    fireSound = "fire2";                    
                }

                else if (type == 101)
                {
                    crackerType = type;
                    imgname = 'images/crackers/anar.png';
                    multi == 0;
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 90
                    };
                    crackerpoint = {
                        x: imageposition.x + 10,
                        y: imageposition.y - 40
                    };
                    burn_image.src = "images/crackers/anarburn.png";
                    anarType = 1;
                }
                else if (type == 102)
                {
                    crackerType = type;
                    imgname = 'images/crackers/anar.png';
                    multi == 0;
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 90
                    };
                    crackerpoint = {
                        x: imageposition.x + 10,
                        y: imageposition.y - 40
                    };
                    burn_image.src = "images/crackers/anarburn.png";
                    anarType = 2;
                }

                else if (type == 103)
                {
                    crackerType = type;
                    imgname = 'images/crackers/anar.png';

                    multi == 0;
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 90
                    };
                    crackerpoint = {
                        x: imageposition.x + 10,
                        y: imageposition.y - 40
                    };
                    burn_image.src = "images/crackers/anarburn.png";
                    anarType = 3;
                }

                else if (type == 104)
                {
                    crackerType = type;
                    imgname = 'images/crackers/anar.png';

                    multi == 0;
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 90
                    };
                    crackerpoint = {
                        x: imageposition.x + 10,
                        y: imageposition.y - 40
                    };
                    burn_image.src = "images/crackers/anarburn.png";
                    anarType = 4;
                }
                else if (type == 105)
                {
                    crackerType = type;
                    imgname = 'images/crackers/anar.png';
                    multi == 0;
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 90
                    };
                    crackerpoint = {
                        x: imageposition.x + 10,
                        y: imageposition.y - 40
                    };
                    burn_image.src = "images/crackers/anarburn.png";
                    anarType = 5;
                }
                else if (type == 106)
                {
                    crackerType = type;
                    imgname = 'images/crackers/anar.png';
                    multi == 0;
                    imageposition = {
                        x: viewportWidth / 2,
                        y: viewportHeight - 90
                    };
                    crackerpoint = {
                        x: imageposition.x + 10,
                        y: imageposition.y - 40
                    };
                    burn_image.src = "images/crackers/anarburn.png";
                    anarType = 6;

                }
                base_image.src = imgname;
                base_image.onload = function ()
                {
                    mainContext.drawImage(base_image, imageposition.x, imageposition.y);
                    mainContext.drawImage(stick_image, 3 * viewportWidth / 4 - 130, viewportHeight / 4 - 10);
                }
                mainCanvas.style.cursor = "url('images/crackers/cursor_hand.png'),auto";
                mousePosition = {
                    x: 3 * viewportWidth / 4,
                    y: viewportHeight / 4
                };
                imageOffset = {
                    x: 3 * viewportWidth / 4 - 130,
                    y: viewportHeight / 4 - 10
                };
                //console.log("initial value is"+ imageOffset.x);
                mouseactive = 1;
            }
            else if (devent == "fireRocket" && crackerType > 0)
            {
                //stop mouse activity at each computer	 		 	
                finish = 0;
                mouseactive = 0;
                clear_on = 1;
                animation_on = 1;
                dragging = false;
                mainCanvas.style.cursor = "default";

                //all vars are ready
                flame.src = 'images/crackers/fire1.png';
                flame.onload = function ()
                {
                    //console.log("spark="+spark.x);
                    mainContext.drawImage(flame, spark.x, spark.y);
                }
                setTimeout(function ()
                {
                    createFirework();
                }, 500);
            }
            else if (devent == "fireAnar" && crackerType > 0)
            {
                finish = 0;
                clear_on = 1;
                animation_on = 1;
                mouseactive = 0;
                dragging = false;
                mainCanvas.style.cursor = "default";
                createAnar();
            }
        });  //socket end
        mainContext.fillStyle = "rgba(0,0,0,1)"; //first time clearing
        mainContext.fillRect(0, 0, viewportWidth, viewportHeight);
        // and now we set off
        //update();
    }


/*
Drawing Anar
*/
    function createAnarParticles(color, pos)
    {
        //green 336 ,924 purple yellow 72888 neela 192 yellowpale yello 264 pink 1140dark green 432 s588 skyblue132 golden colour 948 purple
        rawcount=30;
        if(anarType==3)
        {
            rawcount=50;
        }
        if (anarType == 6)
        {
            rawcount=50;
            anarPattern++;
            //console.log("pattern out");
            if (anarPattern % 50 == 0)
            {
                //console.log("pattern");
                colorIndex++;
                if (colorIndex == 10) colorIndex = 0;
                color = anar_color_array[colorIndex];
            }
            else
            {
                color = anar_color_array[colorIndex];
            }
        }
        var count = 10;
        if (stopanar != 1)
        {
            if (anarVel < 0.4)
            {
                anarVel += 0.01;
            }
            else if (anarVel < 4 && anarVel > 0.2)
            {
                anarVel += 0.15;
                count = rawcount;
            }
            else
            {
                count = rawcount;
            }
        }
        else if (stopanar == 1)
        {
            //count-=0.15;
            //console.log("time to off");
            count = 10;
            if (anarVel > 0.04) anarVel -= 0.04;

        }
        //console.log("I am here at speed="+anarVel);
        var angle = (Math.PI * 2) / count * 0.01;        
        if(anarType==1)
        {
        	//angle = 100*angle;
        }
        var randomVelocity = 3 + anarVel;                 
        if(anarType==6 )
        {
            randomVelocity-=2;
        }
        var particleAngle=0;   
        //console.log("value of color is"+color);
        //color=72; 
        while (count--)
        {   
            if(anarType==6)      
            {
                particleAngle = (Math.PI/ 2)*3/10*count;   
            }

            Fireworks.createParticle(
            pos,
            null,
            {
                x: Math.sin(particleAngle) * randomVelocity,
                y: -Math.cos(angle) * randomVelocity - randfunc() * 1.4
            },
            color,
            true);
        }

    }
    function createAnar()
    {
        if (anarType == 6) 
        	soundManager.start('anarlong');
        else 
        	soundManager.start('anar1');
        flow();
        update();
    }

    function flow()
    {
        anarVel = 0, stopanar = 0;

        var duration = 300;
        if (anarType == 6)
        {
            duration = 1000;
            anarPattern = 0, colorIndex = 0;
        }
        var stopage = duration * 75 / 100;
        for (var i = 0; i < duration; i++)
        {
            setTimeout(
            function ()
            {
                if (anarType == 1) 
                {
                	 createAnarParticles(Math.floor(Math.random() * 100) * 12,
                   {
	                    x: viewportWidth / 2 + 30,
  	                  y: viewportHeight - 70
    		           });
              	}
                else if (anarType == 2)
                {
                    createAnarParticles(72,
                    {
                        x: viewportWidth / 2 + 30,
                        y: viewportHeight - 70
                    });
                }
                else if (anarType == 3)
                {
                    createAnarParticles(1,
                    {
                        x: viewportWidth / 2 + 30,
                        y: viewportHeight - 70
                    });
                }
                else if (anarType == 4)
                {
                    createAnarParticles(432,
                    {
                        x: viewportWidth / 2 + 30,
                        y: viewportHeight - 70
                    });
                }
                else if (anarType == 5)
                {
                    createAnarParticles(924,
                    {
                        x: viewportWidth / 2 + 30,
                        y: viewportHeight - 70
                    });
                }
                else if (anarType == 6)
                {
                    createAnarParticles(1,
                    {
                        x: viewportWidth / 2 + 30,
                        y: viewportHeight - 70
                    });
                }

            }, 100 * i);
        }
        setTimeout(function ()
        {
            stopanar = 1;
        }, stopage * 100);

    }

    /*
Anar Ends
*/

    /**
     * Pass through function to create a
     * new firework on touch / click
     */
    function launch()
    {
        if (crackerType <= 100) 
        {
            socket.emit(user.gameid,
            {
                event: "fireRocket"
            });
        }
        else if (crackerType > 100) 
        {
             socket.emit(user.gameid,
            {
                event: "fireAnar"
            });
        }   
    }

    function createFirework()
    {

        soundManager.start(fireSound);
        createParticle();
        if (multi == 1)
        {
            for (i = 0; i < 6; i++)
            setTimeout(function ()
            {
                soundManager.start(fireSound);
                createParticle();
            }, 1000); //or direct 1000			
        }
        else if (multi == 2)
        {
            for (i = 0; i < 50; i++)
            setTimeout(function ()
            {
                soundManager.start(fireSound);
                createParticle();
                stoppage++;
            }, 1000 * (i + 1) + randfunc() * 4000);
        }

        else if (multi == 3)
        {
            for (i = 0; i < stopset; i++)
            {
                setTimeout(function ()
                {

                    soundManager.start(fireSound);
                    /*
					
					{x: viewportWidth * 0.5,y:viewportHeight -150}
					*/

                    for (i = -4; i < 6; i++)
                    {
                        setTimeout(positionFire(i), 200); //or direct 1000			
                    }
                    stoppage++;

                    if (circle == 1)
                    {
                        circle = 0;
                    }
                    else
                    {
                        circle = 1;
                    }
                    if (stoppage % 3 == 0) fireSound = "fire2";
                    else fireSound = "fire1";


                    console.log("circle is" + circle);
                    console.log("stoppage" + stoppage);
                }, 4000 * (i + 1) + randfunc() * 2000);
            }
        }
        update();
    }

    function positionFire(pos)
    {
        createParticle(
        {
            x: viewportWidth * 0.5 + (pos * 100),
            y: viewportHeight - 150
        });
    }
    /**
     * Creates a block of colours for the
     * fireworks to use as their colouring
     */
    function createFireworkPalette(gridSize)
    {

        var size = gridSize * 10;
        fireworkCanvas.width = size;
        fireworkCanvas.height = size;
        fireworkContext.globalCompositeOperation = 'source-over';

        // create 100 blocks which cycle through
        // the rainbow... HSL is teh r0xx0rz
        for (var c = 0; c < 100; c++)
        {

            var marker = (c * gridSize);
            var gridX = marker % size;
            var gridY = Math.floor(marker / size) * gridSize;

            fireworkContext.fillStyle = "hsl(" + Math.round(c * 3.6) + ",100%,60%)";
            fireworkContext.fillRect(gridX, gridY, gridSize, gridSize);
            fireworkContext.drawImage(
            Library.bigGlow,
            gridX,
            gridY);
        }


    }

    /**
     * Update the canvas based on the
     * detected viewport size
     */
    function setMainCanvasDimensions()
    {
        mainCanvas.width = viewportWidth;
        mainCanvas.height = viewportHeight;
    }

    /**
     * The main loop where everything happens
     */
    function update()
    {

        if (clear_on)
        {
            clearContext();
        }

        if (animation_on == 1)
        {
            //update_running=1;
            requestAnimFrame(update);
            drawFireworks();
        }
    }

    /**
     * Clears out the canvas with semi transparent
     * black. The bonus of this is the trails effect we get
     */
    function clearContext()
    {        
        mainContext.fillStyle = "rgba(0,0,0,0.2)";        
        if(crackerType ==106 ||crackerType==103||(crackerType>10&&crackerType<20))   //for magic anar particales has to be smaller
        {
            mainContext.fillStyle = "rgba(0,0,0,1)";        
        }        
        mainContext.fillRect(0, 0, viewportWidth, viewportHeight);
        if (crackerType > 100) //anar
        {            
            mainContext.drawImage(burn_image, viewportWidth / 2, viewportHeight - 90);
        }
    }

    /**
     * Passes over all particles particles
     * and draws them
     */
    function drawFireworks()
    {
        var a = particles.length;
        //console.log("no. of active particles are"+particles.length);

        if (a < 10 && a > 0 && holdRocket == 0)
        {
            console.log("stoppage=" + stoppage);
            if (multi != 2 && multi != 3)
            {
                if (particles[0].usePhysics)
                {
                    //console.log("time to switch off");
                    particles = [];
                    mainContext.fillStyle = "rgba(0,0,0,1)";
                    mainContext.fillRect(0, 0, viewportWidth, viewportHeight);
                    if (crackerType > 100)
                    {
                        mainContext.drawImage(burn_image, viewportWidth / 2, viewportHeight - 90);
                    }
                    animation_on = 0;
                    finish = 1;
                    socket.emit(user.gameid,
                    {
                        event: "feedback"
                    });
                    console.log("Turning off");
                    //break;
                    a = 0;
                }
            }
            else if ((multi == 2 || multi == 3) && stoppage >= stopset)
            {

                setTimeout(function ()
                {
                    particles = [];
                    mainContext.fillStyle = "rgba(0,0,0,1)";
                    mainContext.fillRect(0, 0, viewportWidth, viewportHeight);
                    animation_on = 0;
                    finish = 1;
                    socket.emit(user.gameid,
                    {
                        event: "feedback"
                    });
                    console.log("Turning off");
                    //break;
                    a = 0;
                }, 3000);
            }
        }

        while (a--)
        {
            var firework = particles[a];

            // if the update comes back as true
            // then our firework should explode
            var y = firework.pos.y,
                x = firework.pos.x;

            if (y < 0 || x < 0 || y > viewportHeight || x > viewportWidth)
            {
                particles.splice(a, 1);
            }

            if (firework.update())
            {

                // kill off the firework, replace it
                // with the particles for the exploded version
                particles.splice(a, 1);
                // if the firework isn't using physics
                // then we know we can safely(!) explode it... yeah.
                if (!firework.usePhysics)
                {

                    if (holdRocket == 1) 
                    {
                        soundManager.stop(fireSound);                                                 
                    }                           
                    soundManager.start('blast');
                    if (holdRocket == 1)
                    {
                        clear_on = 0;
                        setTimeout(function ()
                        {
                            particles = [];
                            console.log("stoped");
                            hold = 1;
                            clear_on = 0;
                        }, 700);
                        setTimeout(function ()
                        {
                            hold = 0;
                            clear_on = 1;
                            animation_on = 0;

                            for (var start = 1; start < 20; start++)
                            setTimeout(function ()
                            {
                                clearContext();
                            }, 200 * start);

                            setTimeout(function ()
                            {
                                console.log("ready");
                                finish = 1;
                                socket.emit(user.gameid,
                                {
                                    event: "feedback"
                                });
                            }, 200 * start);
                        }, 2000);
                    }
                    if (circle == 0)
                    {

                        FireworkExplosions.star(firework);
                    }
                    else
                    {
                        FireworkExplosions.circle(firework);
                    }
                }
            }

            // pass the canvas context and the firework
            // colours to the
            firework.render(mainContext, fireworkCanvas);
        }
    }

    /**
     * Creates a new particle / firework
     */
    function createParticle(pos, target, vel, color, usePhysics, brake)
    {

        pos = pos || {};
        target = target || {};
        vel = vel || {};
        particles.push(
        new Particle(
        // position
        {
            x: pos.x || viewportWidth * 0.5,
            y: pos.y || viewportHeight - 150
        },

        // target
        {
            y: target.y || viewportHeight / 3 - randfunc() * 100
        },

        // velocity
        {
            x: vel.x || randfunc() * 3 - 1.5,
            y: vel.y || 0
        },

        color || Math.floor(randfunc() * 100) * 12,

        usePhysics, brake));
    }

    /**
     * Callback for window resizing -
     * sets the viewport dimensions
     */
    function onWindowResize()
    {
        viewportWidth = window.innerWidth;
        viewportHeight = window.innerHeight - 85;
    }

    // declare an API
    return {
        initialize: initialize,
        createParticle: createParticle,
        randfunc: randfunc
    };

})();
