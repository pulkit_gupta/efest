

var dragging = false,
    imageOffset = {
        x: 0,
        y: 0
    },
    mousePosition;

function dragImage(coords) {
    console.log("mouse moved and corodinates are"+coords.x+","+coords.y);
    imageOffset.x += mousePosition.x - coords.x;
    imageOffset.y += mousePosition.y - coords.y;

    // constrain coordinates to keep the image visible in the canvas
    imageOffset.x = Math.min(0, Math.max(imageOffset.x, canvas.width - imageWidth));
    imageOffset.y = Math.min(0, Math.max(imageOffset.y, canvas.height - imageHeight));

    mousePosition = coords;
}

function drawImage() {
	
    // draw at the position recorded in imageOffset
    // don't forget to clear the canvas before drawing
}

function getMouseCoords(e) {
    // return the position of the mouse relative to the top left of the canvas
     return {x: e.offsetX, y: e.offsetY}
}

document.onmousedown = function(e) {
    dragging = true;
    mousePosition = getMouseCoords(e);
};
document.onmouseup = function() {
    dragging = false;
};
document.onmousemove = function(e) {
    if(dragging) 
    {
    	dragImage(getMouseCoords(e));    	
    }
};