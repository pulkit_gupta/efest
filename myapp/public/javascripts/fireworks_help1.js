
/**
 * Represents a single point, so the firework being fired up
 * into the air, or a point in the exploded firework
 */
var Particle = function (pos, target, vel, marker, usePhysics, brake)
{

    // properties for animation
    // and colouring    
    this.brakeangle = brake || 0;
    this.GRAVITY = 0.06;
    this.alpha = 1;
    this.easing = Fireworks.randfunc() * 0.02;    
    this.fade = Fireworks.randfunc() * 0.1;
    this.gridX = marker % 120;
    this.gridY = Math.floor(marker / 120) * 12;
    this.color = marker;

    this.pos = {
        x: pos.x || 0,
        y: pos.y || 0
    };

    this.vel = {
        x: vel.x || 0,
        y: vel.y || 0
    };

    this.lastPos = {
        x: this.pos.x,
        y: this.pos.y
    };

    this.target = {
        y: target.y || 0
    };

    this.usePhysics = usePhysics || false;

};

/**
 * Functions that we'd rather like to be
 * available to all our particles, such
 * as updating and rendering
 */ 
Particle.prototype = {

    update: function ()
    {

        this.lastPos.x = this.pos.x;
        this.lastPos.y = this.pos.y;

        if (this.usePhysics)
        {            
            if (hold)
            {
                //console.log("hold is 1");
                this.vel.y += this.GRAVITY;
                this.pos.x += this.vel.x / 7;
                this.pos.y += this.vel.y / 7;
                this.alpha = 0.007; //down the alpha for still poz
            }            
            else
            {

                if(speedmodes==1)
                {                       
                    if(this.brakeangle!=1000) 
                    {                        
                        if(this.vel.x > 0)
                        {
                            this.vel.x -= Math.cos(this.brakeangle)*this.GRAVITY;
                            if(this.vel.x<=0)
                            {
                                this.vel.x=0;
                                this.fade*=5;
                                this.brakeangle=1000;
                            }
                        }
                        else
                        {                        
                            this.vel.x -= Math.cos(this.brakeangle)*this.GRAVITY;
                            if(this.vel.x>=0)
                            {
                                this.vel.x=0;
                                this.fade*=5;
                                this.brakeangle=1000;
                            }
                        }
                        //////////////////same with y
                        if(this.vel.y > 0)
                        {
                            this.vel.y -= Math.sin(this.brakeangle)*this.GRAVITY;
                            if(this.vel.y<=0)
                            {
                                this.vel.y=0;
                                this.fade*=5;
                                this.brakeangle=1000;
                            }
                        }
                        else
                        {                        
                            this.vel.y -= Math.sin(this.brakeangle)*this.GRAVITY;
                            if(this.vel.y>=0)
                            {
                                this.vel.y=0;
                                this.fade*=5;
                                this.brakeangle=1000;                          
                            }
                        }                    
                    }
                    else   //no brake angle
                    {                        
                        this.vel.y=0;
                        this.vel.x=0;

                    }
                }
                else  //no speed mode
                {
                    this.vel.y += this.GRAVITY;                    
                }   
                this.pos.x += this.vel.x;                
                this.pos.y += this.vel.y;
            }            //no hold

            // since this value will drop below
            // zero we'll occasionally see flicker,
            // ... just like in real life! Woo! xD
            if(this.brakeangle==1000||speedmodes==0)
            {
                this.alpha -= this.fade;
            }
        }
        else
        {
            var distance = (this.target.y - this.pos.y);

            // ease the position
            this.pos.y += distance * (0.03 + this.easing);
            // cap to 1
            this.alpha = Math.min(distance * distance * 0.00005, 1);
            this.pos.x += this.vel.x;
        }
        return (this.alpha < 0.005);
    },

    render: function (context, fireworkCanvas)
    {

        var x = Math.round(this.pos.x),
            y = Math.round(this.pos.y),
            xVel = (x - this.lastPos.x) * -5,
            yVel = (y - this.lastPos.y) * -5;

        context.save();
        context.globalCompositeOperation = 'lighter';
        context.globalAlpha = Fireworks.randfunc() * this.alpha;

        // draw the line from where we were to where
        // we are now
        context.fillStyle = "rgba(255,255,255,0.3)";
        context.beginPath();
        context.moveTo(this.pos.x, this.pos.y);
        context.lineTo(this.pos.x + 1.5, this.pos.y);
        context.lineTo(this.pos.x + xVel, this.pos.y + yVel);
        context.lineTo(this.pos.x - 1.5, this.pos.y);
        context.closePath();
        context.fill();

        // draw in the images
        context.drawImage(fireworkCanvas,
        this.gridX, this.gridY, 12, 12,
        x - 6, y - 6, 12, 12);
        context.drawImage(Library.smallGlow, x - 3, y - 3);

        context.restore();
    }

};

/**
 * Stores references to the images that
 * we want to reference later on
 */
var Library = {
    bigGlow: document.getElementById('big-glow'),
    smallGlow: document.getElementById('small-glow')
};

/**
 * Stores a collection of functions that
 * we can use for the firework explosions. Always
 * takes a firework (Particle) as its parameter
 */
var FireworkExplosions = {

    /**
     * Explodes in a roughly circular fashion
     */
    circle: function (firework)
    {

        var count = 100;
        if(speedmodes==1)
        {
            count = 800;
        }
        var angle = (Math.PI * 2) / count;
        var particleAngle, randomVelocity;
        while (count--)
        {
            if(speedmodes==1)
            {
                randomVelocity = 1 + Fireworks.randfunc() * 4;
            }
            else                
            {
                randomVelocity = 4 + Fireworks.randfunc() * 4;
            }
            particleAngle = count * angle;

            Fireworks.createParticle(
            firework.pos,
            null,
            {
                x: Math.cos(particleAngle) * randomVelocity,
                y: Math.sin(particleAngle) * randomVelocity
            },
            firework.color,
            true, particleAngle);
        }
    },

    /**
     * Explodes in a star shape
     */
    star: function (firework)
    {

        // set up how many points the firework
        // should have as well as the velocity
        // of the exploded particles etc
        var points = 6 + Math.round(Fireworks.randfunc() * 15);
        var jump = 3 + Math.round(Fireworks.randfunc() * 7);
        var subdivisions = 10;
        var radius = 80;
        var randomVelocity = -(Fireworks.randfunc() * 3 - 6);

        var start = 0;
        var end = 0;
        var circle = Math.PI * 2;
        var adjustment = Fireworks.randfunc() * circle;

        do {

            // work out the start, end
            // and change values
            start = end;
            end = (end + jump) % points;

            var sAngle = (start / points) * circle - adjustment;
            var eAngle = ((start + jump) / points) * circle - adjustment;

            var startPos = {
                x: firework.pos.x + Math.cos(sAngle) * radius,
                y: firework.pos.y + Math.sin(sAngle) * radius
            };

            var endPos = {
                x: firework.pos.x + Math.cos(eAngle) * radius,
                y: firework.pos.y + Math.sin(eAngle) * radius
            };

            var diffPos = {
                x: endPos.x - startPos.x,
                y: endPos.y - startPos.y,
                a: eAngle - sAngle
            };

            // now linearly interpolate across
            // the subdivisions to get to a final
            // set of particles
            for (var s = 0; s < subdivisions; s++)
            {

                var sub = s / subdivisions;
                var subAngle = sAngle + (sub * diffPos.a);

                Fireworks.createParticle(
                {
                    x: startPos.x + (sub * diffPos.x),
                    y: startPos.y + (sub * diffPos.y)
                },
                null,
                {
                    x: Math.cos(subAngle) * randomVelocity,
                    y: Math.sin(subAngle) * randomVelocity
                },
                firework.color,
                true);
            }

            // loop until we're back at the start
        } while (end !== 0);

    }

};