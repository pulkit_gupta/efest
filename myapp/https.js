
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')  
  , http = require('http')
  , https = require('https')
  , path = require('path')
  , member = require('./routes/member')
  , io     = require('socket.io')
  , fs =require('fs');
  
var util = require('util'); 
var config = require('./config').Config; 
var mysql = require('mysql');

var app = express();


var options = {
    key:    fs.readFileSync('ssl/efest.key').toString(),
    cert:   fs.readFileSync('ssl/efest_in.crt').toString(),
    ca: [fs.readFileSync('ssl/AddTrustExternalCARoot.crt').toString(),fs.readFileSync('ssl/PositiveSSLCA2.crt').toString()],    
    passphrase: 'ankur1'
};

var server2 = https.createServer(options, app);

//var Memstore = express.session.MemoryStore;
var RedisStore = require ( 'connect-redis' ) ( express ),
sessionStore = new RedisStore ();

var logFile = fs.createWriteStream('./efesthttp.log',{flags: 'a'});

app.configure(function(){  
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  //app.use(express.favicon());
  app.use(express.errorHandler(
      { dumpExceptions: true, showStack: true })); 
  app.use(express.favicon(__dirname + '/public/favicon.ico', { maxAge: 2592000000 }));  
  app.use(express.logger({stream: logFile}));
  app.use(express.bodyParser());
  app.use(express.methodOverride());  
  app.use(require('stylus').middleware(__dirname + '/public'));
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(express.cookieParser()); 
  
 /* app.use(express.session({ secret: 'somerandomstring', store: Memstore({
  	reapInterval: 60000*10
  
  })}));*/
  app.use ( express.session ( {
      secret: "keyboard cat", store: sessionStore, key: 'hello.sid'
   } ) );
  
   app.use(app.router);
});

app.configure('development', function(){
  app.use(express.errorHandler());
});


var connection = mysql.createConnection({
  	host     : 'localhost',
  	user     : 'root',
    	password : config.dbPassword,
    	database : config.dbName
  });
  
  
function requiresLogin(req, res, next){
	if(req.session.user){
	console.log("session is there");
	next();
	}
	else{
	console.log("session is not there");
	res.redirect('/login?redir='+req.url);
	
	}
}
/* Controllers //////////////////////////////////////////////////////////////////////////////////////////////////*/

app.get('/', routes.index);
app.get('/browser', routes.browser);
app.get('/contact', routes.contact);
app.get('/register', user.signup);
app.post('/userconfirm', user.confirm);  
app.get('/activate', user.activate);
app.post('/loginuser', user.loginuser);
app.get('/login', user.login);
app.get('/logout', function(req, res){
	if(req.session.user)
	delete req.session.user;
	
	res.redirect('login');
});
app.get('/forgot', member.forgot);
app.post('/forgotprocess', member.forgotprocess);
app.get('/resetpassword', member.resetpassword);   //a onetime link
app.post('/newpassword', member.newpassword);

app.get('/member',requiresLogin, member.page);
app.get('/settings',requiresLogin, member.settings);
app.post('/update',requiresLogin, user.update);
app.get('/mycelebrations', requiresLogin, member.mycelebrations);

app.get('/plans',member.plans);
app.post('/pay',member.pay);  


app.get('/play',member.play);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

server2.listen(443, function(){
  console.log("Express server listening on port " + config.socketPort);        
  console.log("environment started is "+config.env);
  console.log("URL is "+config.appURL);    
  console.log("Time zone with respect to UTC is "+config.timeZone);    
 //testing new code   	     
  // testing over
});


