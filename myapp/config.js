
var development = {
  appURL : 'localhost',
  timeZone: 5.5,
  socketPort : 80,
  dbName: 'diwali',
  dbPassword: '',
    
  mailKey: "7c745ce4-73c0-49ce-a5ad-e119e295445d",
  siteEmail: "info@efest.in",

   paymentAction: "https://test.direcpay.com/direcpay/secure/dpMerchantTransaction.jsp",
  mid: "200904281000001",
  toml: "TOML",
  
  env : global.process.env.NODE_ENV || 'development'
};



/*
Payment gateway testing
var rqp ="200904281000001|DOM|IND|INR|"+inr+"|efest.in"+insertid+"|NULL|http://efest.in/status|http://efest.in/status|TOML";					
var addr = "https://test.direcpay.com/direcpay/secure/dpMerchantTransaction.jsp";
TOML

Payment live
var rqp ="200904281000001|DOM|IND|INR|"+inr+"|efest.in"+insertid+"|NULL|http://efest.in/status|http://efest.in/status|TOML";					
var addr = "https://timesofmoney.com/direcpay/secure/dpMerchantTransaction.jsp";
*/
var production = {
  appURL : 'www.efest.in',
  timeZone: 5.5,
  socketPort : 3000,  
  dbName: 'diwali',
  dbPassword: 'pulkit$2212',
  
  mailKey: "7c745ce4-73c0-49ce-a5ad-e119e295445d",
  siteEmail: "info@efest.in",
  
  paymentAction: "https://www.timesofmoney.com/direcpay/secure/dpMerchantTransaction.jsp",
  mid: "201211081000003",
  toml: "DirecPay",
  
  
  env : global.process.env.NODE_ENV || 'development'
};

exports.Config = global.process.env.NODE_ENV === 'production' ? production : development;