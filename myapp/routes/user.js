
/*
 * GET users listing.
 */

var config = require('../config').Config; 

var postmark = require("postmark")(config.mailKey);
var fs=require('fs');
var mysql = require('mysql');
var connection;
reconnect();
function reconnect()
{
	console.log('Trying to connect sql. in User.js');
	connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
      password : config.dbPassword,
      database : config.dbName
  	});  	
  	connection.connect(function(err)
	{
		if(err)
		{
			console.log("failed");
			reconnect();
		}
		else
		{
			console.log("connected");
			connection.on('error', function(err) 
			{
			   /* if (!err.fatal) {
			      return;
			    }*/
				if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
			      //throw err;
			      console.log("Error is coontinous. and can not be solved.");
			    }
			    else
			    {
			    	console.log("Retrying!!unexpected connection lost.");
			    	reconnect();
			    }    
			});

		}

	});	    
}


var hash = require('node_hash');	

exports.signup = function(req, res){
	if(req.session.user){
	 	console.log("session is there you must go to members page"); 	
	 	res.redirect('/member');
	}
	else{ 			
		res.render('register', { title: 'Register' });
	}	
	
	
};

exports.confirm = function(req, res){
	
	
	
	var str1 = req.body.firstname;
	var str2 = req.body.lastname;
	var str3 = req.body.street||"";
	var str4 = req.body.address2||"";
	var str5 = req.body.city||"";
	var str6 = req.body.state||"";
	var str7 = req.body.postal||"";
	var str8 = req.body.country||"";
	var str9 = req.body.email;
	var str10 = req.body.mobile||"";
	var str11 = 0;   //active
	var str12 = hash.md5(String(str9));	//passwordrest			
	var str13 = req.body.password;
	var date = new Date();
	str14 = date.getTime() - date.getTimezoneOffset() * 60*1000;  //timestamp
	salt = String(str14);
	
	
        str13 = hash.md5(String(str13), salt);	               
		
	var sql    = 'SELECT * FROM users WHERE email='+connection.escape(str9);;
	connection.query(sql, function(err, results) {
		if(results.length==1)
		{
		
				res.render('login', { locals: {title: "Login"}, msg: 'You are already registered with this email ID', redir: "/member"});
		}
		else	
		{				
			connection.query('INSERT INTO users SET ?', {firstname: str1 , lastname: str2 , street: str3 , address2: str4 , city: str5 , state: str6 , postal: str7 , country: str8 , email: str9 , mobile: str10 , active: str11 , passwordreset: str12, password: str13, timestamp: str14}, function(err, result) 
			{
				if(req.files.photo.size!=0)
				{
					fs.readFile(req.files.photo.path, function (err, data) 
					{								
						if (err) 
						{//throw err;
						}
						//insert image upload	
						fs.readFile(req.files.photo.path, function (err, data) 
						{		
							var newPath ="public/users/"+str9+".jpg";
							console.log("new path is "+ newPath);
				       			fs.writeFile(newPath, data, function (err) 
				       			{       		       			       
				       				if(err)
				       				console.log(err);       	
							});
				  
						});
						console.log(result.insertId);
					});
				}
				else
				{
					var fsa = require('fs-extra');
					fsa.copy("public/users/userg.jpg","public/users/"+str9+".jpg", function(err) {
						if(!err)
						console.log("green copied");
					});

				}
			
				/*var email = require("mailer");
				email.send({
					host : "smtp.gmail.com", // smtp server hostname
					port : "465", // smtp server port
					ssl: true,	 // for SSL support - REQUIRES NODE v0.3.x OR HIGHER
					domain : "smtp.gmail.com", // domain used by client to identify itself to server
					to : str9,
					from : "eutsava@gmail.com",
					subject : "Activation link",
					html: "click here to activate your account http://"+config.appURL+"/activate?q="+str12,
					authentication : "login", // auth login is supported; anything else is no auth
					username : "eutsava@gmail.com", // username
					password : "efest.in" // password
					},
					function(err, result){
						if(err){ console.log(err);
						console.log(result);
						}
					}
				);//send email	  */
				
				postmark.send({
				    "From": config.siteEmail, 
				    "To": str9,
				    "Subject": "Acount activation link", 
				    "HtmlBody": "Dear User, <br><br>Please click here to activate your account http://"+config.appURL+"/activate?q="+str12+" If link not avaliable, Copy it in other tab of your web browser to activate your account.<br><br><br>regards, <br>Team "+config.appURL
				}, function (err, to) {
				    if (err) {
				        console.log(err);
				        return;
				    }
				    console.log("Email sent to: %s", to);
				});
				
				
				
				
				res.render('login', { msg: 'Email Confirmation will be sent to your email ID with in next 5 Minutes. Please click that link before login',redir: "/member" });
			});
		} //else
	});//sql
		
};



exports.activate = function(req, res){
	
	
	var sql= 'SELECT * FROM users WHERE active = 0 AND passwordreset = ' + connection.escape(req.query.q);
	connection.query(sql, function(err, results) {	
			
		if(results[0]==undefined)
		{			
			res.render('login', { msg: 'Link has expired', redir: "/member"});	
		}
		
		else
		{
			var sql= 'UPDATE users SET active=1 WHERE active = 0 AND passwordreset = ' + connection.escape(req.query.q);
			connection.query(sql, function(err, results) {	
				res.render('login', { msg: 'Account activated', redir: "/member"});
			});
		}		
	});

};


exports.login = function(req, res){
	
	if(req.session.user){
		
		console.log("session is there you must go to members page"); 			
	 	res.redirect('/member');
	}
	else{ 			
		res.render('login', {locals: {title: "Login"}, msg: '', redir: req.query.redir});
	}
	
};


exports.loginuser = function(req, res){
		
	//console.log(connection);
	//connection = sqlCon();
	var sql= 'SELECT * FROM users WHERE active =1 AND email = ' + connection.escape(req.body.email);	
		connection.query(sql, function(err, results) {				
				
			if(results[0]==undefined)
			{			
				res.render('login', { locals: {title: "Login"}, msg: 'EMail ID entered is not registered or activated.', redir: req.body.redir});	
			}
			
			else
			{
				var pass = req.body.password;
				var salt = results[0].timestamp;
				var email= results[0].email;			
				var sql= 'SELECT firstname, lastname, street, address2, city, state, postal, country, mobile, email FROM users WHERE email = '+connection.escape(email)+'AND password = ' + connection.escape(hash.md5(String(req.body.password),String(salt)));
				connection.query(sql, function(err, results) {	
					if(results[0]==undefined)
					{
						console.log("null");
						res.render('login', { locals: {title: "Login"}, msg: 'Invalid Password!', redir: req.body.redir});
					}
					else
					{	
						req.session.user = results[0];
										
						////////////////////////////////////////////////////////////////////											
						var redir = "/member";
						if(req.body.redir != "undefined"){
							redir = req.body.redir;
						}
						 
						res.redirect(redir);
					}
				});
			}		
	});
		
};


exports.update = function(req, res){

	var str1 = req.body.firstname;
	var str2 = req.body.lastname;
	var str3 = req.body.street;
	var str4 = req.body.address2;
	var str5 = req.body.city;
	var str6 = req.body.state;
	var str7 = req.body.postal;
	var str8 = req.body.country;
	var str9 = req.session.user.email;  //used to update correct database
	var str10 = req.body.mobile;
	var str13 = req.body.password;	
	var str14 = 0;
	var sql= 'UPDATE users SET firstname='+connection.escape(str1)+',lastname='+connection.escape(str2)+',street='+connection.escape(str3)+',address2='+connection.escape(str4)+',city='+connection.escape(str5)+',state='+connection.escape(str6)+',postal='+connection.escape(str7)+',country='+connection.escape(str8)+',mobile='+connection.escape(str10)+' WHERE email='+ connection.escape(str9);
	if(str13!="")  //if password not empty
	{	
		var date = new Date();
		str14 = date.getTime()-date.getTimezoneOffset() * 60*1000;  //timestamp
		var salt = String(str14);
		str13 = hash.md5(String(str13), salt);	       			
		sql= 'UPDATE users SET firstname='+connection.escape(str1)+',lastname='+connection.escape(str2)+',street='+connection.escape(str3)+',address2='+connection.escape(str4)+',city='+connection.escape(str5)+',state='+connection.escape(str6)+',postal='+connection.escape(str7)+',country='+connection.escape(str8)+',mobile='+connection.escape(str10)+',password='+connection.escape(str13)+',timestamp='+connection.escape(str14)+' WHERE email='+ connection.escape(str9);		
		connection.query(sql, function(err, results) {			
		});	
	}
	connection.query(sql, function(err, results) {	
		if(err)
		console.log(err);
		console.log("done editing");
		
		var sql= 'SELECT firstname, lastname, street, address2, city, state, postal, country, mobile, email FROM users WHERE email = '+connection.escape(str9);
		connection.query(sql, function(err, results) {	
			req.session.user = results[0];
			if(req.files.photo.size!=0){
				fs.readFile(req.files.photo.path, function (err, data) {				
				if(!err)
				{
					var newPath ="public/users/"+str9+".jpg";
					console.log("new path is "+ newPath);
		      				fs.writeFile(newPath, data, function (err) {       		       			       
       						if(err)
       						console.log(err);       	
  					});
  				}	
  				});//read file upload		
  		  	}				
			//res.render('edituser', { session: req.session.user });
			res.redirect('/settings');
		
		}); //DB to update session
	});//sql
	
	
	

};