
/*
 * GET home page.
 */

exports.index = function(req, res){
	if(req.session.user){
	 	console.log("session is there you must go to members page"); 	
	 	res.redirect('/member');
	}
	else{ 			
		res.render('index', { title: 'Home' });
	}
};


exports.contact = function(req, res){
	res.render('contact',{locals: {login: req.session.user, title: "Contact"}});
};

exports.browser = function(req, res){
	res.render('browser');
};

exports.steps = function(req, res){
	res.render('steps', {locals: {login: req.session.user, title: "Steps"}});
};

exports.how = function(req, res){
	res.render('how', {locals: {login: req.session.user, title: "Help"}});
};

exports.terms = function(req, res){
	res.render('terms');
};

exports.giftcoupon = function(req, res){
	res.render('giftcoupon', {locals: {title: "Gift", login: req.session.user}});
};