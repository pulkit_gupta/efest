var config = require('./config').Config; 

var postmark = require("postmark")(config.mailKey);

postmark.send({
			    "From": config.siteEmail, 
			    "To": "pulkit.itp@gmail.com", 
			    "Subject": "Gift coupon for "+config.appURL, 			    
			    "HtmlBody": "Sir / Ma’am,<br><br>Mr. / Mrs. "+"Pulkit"+" has sent a gift coupon for "+"2"+" user(s) for enjoying, watching and burning of crackers / fireworks.<br><br>Log on to "+config.appURL+" with coupon code <font color=red>"+"1234"+"</font>.<br><br>Have a great day !!!!!!!!!!!!!!<br><br><br> regards, <br> Team<br>"+config.appURL,
				}, function (err, to) {
					    if (err) {
					        console.log(err);
					        return;
			 	   }
			 	   console.log("Email sent for payment to: %s", to);
		});		