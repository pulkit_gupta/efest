-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 09, 2012 at 07:08 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `diwali`
--

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `couponid` int(11) NOT NULL AUTO_INCREMENT,
  `code` text NOT NULL,
  `valid` int(11) NOT NULL,
  `no_of_users` int(11) NOT NULL,
  `usedBy` text NOT NULL,
  `giftedby` text NOT NULL,
  `giftedto` text NOT NULL,
  `giftedon` text NOT NULL,
  `paymentid` text NOT NULL,
  PRIMARY KEY (`couponid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`couponid`, `code`, `valid`, `no_of_users`, `usedBy`, `giftedby`, `giftedto`, `giftedon`, `paymentid`) VALUES
(1, 'a1pdk', 1, 0, ', pulkit.itp@gmail.com', '', '', '', ''),
(2, 'tx2w1qa5we4red', 0, 0, ', pulkit.itp@gmail.com, pulkit.itp@gmail.com, pulkit.itp@gmail.com, pulkit.itp@gmail.com', '', '', '', ''),
(3, 'whb34kjhf88', 1, 0, '', '', '', '', ''),
(4, 'bhsd23x65wefn', 1, 0, '', '', '', '', ''),
(5, 'xyz', 0, 2, '', 'pulkit.itp@gmail.com', 'p@p.com', 'Sun, 09 Dec 2012 16:37:58 GMT', ''),
(6, 'xyz', 0, 2, '', 'pulkit.itp@gmail.com', 'p@hmail.com', 'Sun, 09 Dec 2012 16:38:41 GMT', ''),
(7, 'xyz', 0, 2, '', 'pulkit.itp@gmail.com', 'p@p.com', 'Sun, 09 Dec 2012 16:39:59 GMT', ''),
(8, 'xyz', 0, 2, '', 'pulkit.itp@gmail.com', 'p@p.com', 'Sun, 09 Dec 2012 17:16:04 GMT', ''),
(9, 'xyz', 0, 2, '', 'pulkit.itp@gmail.com', 'p@p.com', 'Sun, 09 Dec 2012 17:26:12 GMT', ''),
(10, 'xyz', 0, 3, '', 'pulkit.itp@gmail.com', 'p@p.com', 'Sun, 09 Dec 2012 17:27:27 GMT', ''),
(11, 'xyz', 0, 1, '', 'pulkit.itp@gmail.com', 'p@p.com', 'Sun, 09 Dec 2012 17:32:59 GMT', ''),
(12, 'xyz', 0, 1, '', 'pulkit.itp@gmail.com', 'p@p.com', 'Sun, 09 Dec 2012 17:34:16 GMT', ''),
(13, 'xyz', 0, 5, '', 'pulkit.itp@gmail.com', 'ankit.itp@gmail.com', 'Sun, 09 Dec 2012 22:24:28 GMT', ''),
(14, 'xyz', 0, 1, '', 'pulkit.itp@gmail.com', 'p@p.com', 'Sun, 09 Dec 2012 22:31:15 GMT', ''),
(15, 'xyz', 0, 3, '', 'pulkit.itp@gmail.com', 'p@ppp.com', 'Sun, 09 Dec 2012 22:32:06 GMT', ''),
(16, 'hello', 1, 1, '', 'pulkit.itp@gmail.com', 'p@p.com', 'Sun, 09 Dec 2012 22:36:53 GMT', ''),
(17, '70efdf', 0, 4, ', pulkit.itp@gmail.com', 'pulkit.itp@gmail.com', 'p@pplpl.com', 'Sun, 09 Dec 2012 22:44:48 GMT', ''),
(18, 'xyz', 0, 1, '', 'pulkit.itp@gmail.com', 'pkpk@pl.com', 'Sun, 09 Dec 2012 22:46:50 GMT', ''),
(19, '1f0e3d', 1, 1, '', 'pulkit.itp@gmail.com', 'p@dd.com', 'Sun, 09 Dec 2012 23:07:33 GMT', ''),
(20, '98f137', 0, 1, 'pulkit.itp@gmail.com', 'pulkit.itp@gmail.com', 'p@gmail.com', 'Sun, 09 Dec 2012 23:13:29 GMT', ''),
(21, 'xyz', 0, 3, '', 'pulkit.itp@gmail.com', 'pk@jj.com', 'Sun, 09 Dec 2012 23:34:01 GMT', ''),
(22, 'b6d767', 1, 4, '', 'pulkit.itp@gmail.com', 'k@k.com', 'Sun, 09 Dec 2012 23:35:34 GMT', '1001212000334218'),
(23, 'xyz', 0, 2, '', 'pulkit.itp@gmail.com', 'kk@hh.com', 'Sun, 09 Dec 2012 23:36:48 GMT', '');

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `gameid` int(11) NOT NULL AUTO_INCREMENT,
  `registered_on` text NOT NULL,
  `registered_by` text NOT NULL,
  `no_of_users` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '0',
  `started_on` text NOT NULL,
  `started_string` text NOT NULL,
  `started_by` text NOT NULL,
  `ended_at` text NOT NULL,
  `status` text NOT NULL,
  `payid` text NOT NULL,
  PRIMARY KEY (`gameid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90 ;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`gameid`, `registered_on`, `registered_by`, `no_of_users`, `amount`, `valid`, `started_on`, `started_string`, `started_by`, `ended_at`, `status`, `payid`) VALUES
(1, 'Sun, 04 Nov 2012 13:47:23 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, '1352036847432', 'Sun, 04 Nov 2012 13:47:27 GMT', 'pulkit.itp@gmail.com', 'Sun, 04 Nov 2012 15:30:16 GMT', 'Expired', ''),
(2, 'Sun, 04 Nov 2012 19:39:05 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, '1352057960786', 'Sun, 04 Nov 2012 19:39:20 GMT', 'pulkit.itp@gmail.com', 'Mon, 05 Nov 2012 13:19:14 GMT', 'Expired', ''),
(3, 'Sun, 04 Nov 2012 22:28:03 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, '1352068087200', 'Sun, 04 Nov 2012 22:28:07 GMT', 'pulkit.itp@gmail.com', 'Sun, 04 Nov 2012 23:29:01 GMT', 'Expired', ''),
(4, 'Mon, 05 Nov 2012 13:19:23 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, '1352121566850', 'Mon, 05 Nov 2012 13:19:26 GMT', 'pulkit.itp@gmail.com', 'Mon, 05 Nov 2012 14:35:30 GMT', 'Expired', ''),
(5, 'Mon, 05 Nov 2012 16:20:32 GMT', 'p1@mailinator.com', 1, 100, 1, '1352132436058', 'Mon, 05 Nov 2012 16:20:36 GMT', 'p1@mailinator.com', ' N.A.', 'Running', ''),
(6, 'Mon, 05 Nov 2012 17:36:21 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, '1352137027639', 'Mon, 05 Nov 2012 17:37:07 GMT', 'pulkit.itp@gmail.com', 'Mon, 05 Nov 2012 18:42:22 GMT', 'Expired', ''),
(7, 'Mon, 05 Nov 2012 23:10:00 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, '1352211458502', 'Tue, 06 Nov 2012 14:17:38 GMT', 'pulkit.itp@gmail.com', 'Tue, 06 Nov 2012 15:18:32 GMT', 'Expired', ''),
(8, 'Tue, 06 Nov 2012 15:18:53 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, '1352215136626', 'Tue, 06 Nov 2012 15:18:56 GMT', 'pulkit.itp@gmail.com', 'Tue, 06 Nov 2012 16:19:40 GMT', 'Expired', ''),
(9, 'Tue, 06 Nov 2012 16:20:24 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, '1352218827596', 'Tue, 06 Nov 2012 16:20:27 GMT', 'pulkit.itp@gmail.com', 'Tue, 06 Nov 2012 17:21:21 GMT', 'Expired', ''),
(10, 'Tue, 06 Nov 2012 17:21:48 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, '1352222512576', 'Tue, 06 Nov 2012 17:21:52 GMT', 'pulkit.itp@gmail.com', 'Tue, 06 Nov 2012 18:22:46 GMT', 'Expired', ''),
(11, 'Tue, 06 Nov 2012 18:26:32 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, '1352226396231', 'Tue, 06 Nov 2012 18:26:36 GMT', 'pulkit.itp@gmail.com', 'Tue, 06 Nov 2012 21:54:00 GMT', 'Expired', ''),
(12, 'Wed, 07 Nov 2012 23:50:17 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(13, 'Wed, 07 Nov 2012 23:50:45 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(14, 'Thu, 08 Nov 2012 00:14:14 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(15, 'Thu, 08 Nov 2012 00:15:49 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(16, 'Thu, 08 Nov 2012 00:16:23 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(17, 'Thu, 08 Nov 2012 00:19:07 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(18, 'Thu, 08 Nov 2012 00:21:32 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(19, 'Thu, 08 Nov 2012 00:23:21 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(20, 'Thu, 08 Nov 2012 00:24:55 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(21, 'Thu, 08 Nov 2012 00:28:28 GMT', 'pulkit.itp@gmail.com', 1, 100, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(22, 'Thu, 08 Nov 2012 17:40:23 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(23, 'Thu, 08 Nov 2012 17:40:34 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(24, 'Thu, 08 Nov 2012 17:41:00 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(25, 'Thu, 08 Nov 2012 17:41:15 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(26, 'Thu, 08 Nov 2012 17:41:50 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(27, 'Thu, 08 Nov 2012 17:42:12 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(28, 'Thu, 08 Nov 2012 17:43:09 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(29, 'Thu, 08 Nov 2012 17:43:18 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(30, 'Thu, 08 Nov 2012 17:43:36 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(31, 'Thu, 08 Nov 2012 17:43:44 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(32, 'Thu, 08 Nov 2012 17:44:02 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(33, 'Thu, 08 Nov 2012 17:44:40 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(34, 'Thu, 08 Nov 2012 17:44:59 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(35, 'Thu, 08 Nov 2012 17:45:35 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(36, 'Thu, 08 Nov 2012 17:45:56 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(37, 'Thu, 08 Nov 2012 17:46:32 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(38, 'Thu, 08 Nov 2012 17:48:03 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(39, 'Thu, 08 Nov 2012 17:48:26 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(40, 'Thu, 08 Nov 2012 17:49:08 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(41, 'Thu, 08 Nov 2012 17:49:41 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(42, 'Thu, 08 Nov 2012 17:51:56 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(43, 'Thu, 08 Nov 2012 17:52:01 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(44, 'Thu, 08 Nov 2012 17:52:05 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(45, 'Thu, 08 Nov 2012 17:52:21 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(46, 'Thu, 08 Nov 2012 17:54:14 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(47, 'Thu, 08 Nov 2012 17:54:17 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(48, 'Thu, 08 Nov 2012 17:54:48 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(49, 'Thu, 08 Nov 2012 17:55:34 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(50, 'Thu, 08 Nov 2012 17:59:23 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(51, 'Thu, 08 Nov 2012 17:59:50 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(52, 'Thu, 08 Nov 2012 18:00:55 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(53, 'Thu, 08 Nov 2012 18:01:26 GMT', 'pulkit.itp@gmail.com', 2, 222, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(54, 'Thu, 08 Nov 2012 18:02:33 GMT', 'pulkit.itp@gmail.com', 2, 222, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(55, 'Thu, 08 Nov 2012 18:02:47 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(56, 'Thu, 08 Nov 2012 18:02:58 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, '1354974942904', 'Sat, 08 Dec 2012 13:55:42 GMT', 'pulkit.itp@gmail.com', 'Sat, 08 Dec 2012 16:44:08 GMT', 'Expired', ''),
(57, 'Thu, 08 Nov 2012 18:03:24 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(58, 'Thu, 08 Nov 2012 18:03:43 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(59, 'Thu, 08 Nov 2012 18:04:02 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, '1352639273948', 'Sun, 11 Nov 2012 13:07:53 GMT', 'pulkit.itp@gmail.com', 'Sun, 11 Nov 2012 16:34:20 GMT', 'Expired', ''),
(60, 'Sun, 11 Nov 2012 13:07:34 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(61, 'Sun, 11 Nov 2012 19:40:07 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(62, 'Sun, 11 Nov 2012 19:41:16 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(63, 'Sun, 11 Nov 2012 19:43:15 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(64, 'Sun, 11 Nov 2012 19:43:54 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(65, 'Sun, 11 Nov 2012 19:45:02 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(66, 'Sun, 11 Nov 2012 19:46:14 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(67, 'Sun, 11 Nov 2012 20:40:17 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(68, 'Sun, 11 Nov 2012 21:33:42 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(69, 'Sun, 11 Nov 2012 21:36:04 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, '1354977297342', 'Sat, 08 Dec 2012 14:34:57 GMT', 'pulkit.itp@gmail.com', ' N.A.', 'Running', ''),
(70, 'Sun, 11 Nov 2012 21:36:24 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, '1354819471810', 'Thu, 06 Dec 2012 18:44:31 GMT', 'pulkit.itp@gmail.com', ' N.A.', 'Running', ''),
(71, 'Sun, 11 Nov 2012 21:37:09 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, '1354818686073', 'Thu, 06 Dec 2012 18:31:26 GMT', 'pulkit.itp@gmail.com', ' N.A.', 'Running', ''),
(72, 'Sun, 11 Nov 2012 21:37:27 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, '1354055168570', 'Tue, 27 Nov 2012 22:26:08 GMT', 'pulkit.itp@gmail.com', 'Thu, 06 Dec 2012 18:44:04 GMT', 'Expired', ''),
(73, 'Sun, 11 Nov 2012 21:37:36 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, '1353327255088', 'Mon, 19 Nov 2012 12:14:15 GMT', 'pulkit.itp@gmail.com', 'Mon, 19 Nov 2012 12:14:27 GMT', 'Expired', ''),
(74, 'Sun, 11 Nov 2012 21:37:47 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(75, 'Mon, 19 Nov 2012 12:13:19 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(76, 'Mon, 19 Nov 2012 12:13:43 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, '1353327228236', 'Mon, 19 Nov 2012 12:13:48 GMT', 'pulkit.itp@gmail.com', 'Mon, 19 Nov 2012 12:14:01 GMT', 'Expired', ''),
(77, 'Tue, 04 Dec 2012 23:20:36 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(78, 'Sun, 09 Dec 2012 15:43:08 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(79, 'Sun, 09 Dec 2012 17:20:12 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(80, 'Sun, 09 Dec 2012 17:23:07 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', '1001212000334194'),
(81, 'Sun, 09 Dec 2012 22:23:35 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', '1001212000334203'),
(82, 'Sun, 09 Dec 2012 22:46:00 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(83, 'Sun, 09 Dec 2012 22:46:23 GMT', 'pulkit.itp@gmail.com', 1, 111, 0, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', ''),
(84, 'Sun, 09 Dec 2012 23:08:32 GMT', 'pulkit.itp@gmail.com', 2, 222, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', '1001212000334210'),
(85, 'Sun, 09 Dec 2012 23:19:31 GMT', 'pulkit.itp@gmail.com', 2, 222, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', '1001212000334212'),
(86, 'Sun, 09 Dec 2012 23:21:24 GMT', 'pulkit.itp@gmail.com', 2, 222, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', '1001212000334213'),
(87, 'Sun, 09 Dec 2012 23:23:12 GMT', 'pulkit.itp@gmail.com', 2, 222, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', '1001212000334214'),
(88, 'Sun, 09 Dec 2012 23:25:02 GMT', 'pulkit.itp@gmail.com', 2, 222, 1, 'Not started yet', 'Not started yet', 'N.A.', ' N.A.', 'Ready', '1001212000334215'),
(89, 'Sun, 09 Dec 2012 23:27:44 GMT', 'pulkit.itp@gmail.com', 2, 222, 1, '1355096382599', 'Sun, 09 Dec 2012 23:39:42 GMT', 'pulkit.itp@gmail.com', ' N.A.', 'Running', '1001212000334216');

-- --------------------------------------------------------

--
-- Table structure for table `relation`
--

CREATE TABLE IF NOT EXISTS `relation` (
  `relationId` int(11) NOT NULL AUTO_INCREMENT,
  `gameid` int(11) NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`relationId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

--
-- Dumping data for table `relation`
--

INSERT INTO `relation` (`relationId`, `gameid`, `email`) VALUES
(1, 1, 'pulkit.itp@gmail.com'),
(2, 2, 'pulkit.itp@gmail.com'),
(3, 3, 'pulkit.itp@gmail.com'),
(4, 4, 'pulkit.itp@gmail.com'),
(5, 5, 'p1@mailinator.com'),
(6, 6, 'pulkit.itp@gmail.com'),
(7, 7, 'pulkit.itp@gmail.com'),
(8, 8, 'pulkit.itp@gmail.com'),
(9, 9, 'pulkit.itp@gmail.com'),
(10, 10, 'pulkit.itp@gmail.com'),
(11, 11, 'pulkit.itp@gmail.com'),
(12, 12, 'pulkit.itp@gmail.com'),
(13, 13, 'pulkit.itp@gmail.com'),
(14, 14, 'pulkit.itp@gmail.com'),
(15, 15, 'pulkit.itp@gmail.com'),
(16, 16, 'pulkit.itp@gmail.com'),
(17, 17, 'pulkit.itp@gmail.com'),
(18, 18, 'pulkit.itp@gmail.com'),
(19, 19, 'pulkit.itp@gmail.com'),
(20, 20, 'pulkit.itp@gmail.com'),
(21, 21, 'pulkit.itp@gmail.com'),
(22, 22, 'pulkit.itp@gmail.com'),
(23, 23, 'pulkit.itp@gmail.com'),
(24, 24, 'pulkit.itp@gmail.com'),
(25, 25, 'pulkit.itp@gmail.com'),
(26, 26, 'pulkit.itp@gmail.com'),
(27, 27, 'pulkit.itp@gmail.com'),
(28, 28, 'pulkit.itp@gmail.com'),
(29, 29, 'pulkit.itp@gmail.com'),
(30, 30, 'pulkit.itp@gmail.com'),
(31, 31, 'pulkit.itp@gmail.com'),
(32, 32, 'pulkit.itp@gmail.com'),
(33, 33, 'pulkit.itp@gmail.com'),
(34, 34, 'pulkit.itp@gmail.com'),
(35, 35, 'pulkit.itp@gmail.com'),
(36, 36, 'pulkit.itp@gmail.com'),
(37, 37, 'pulkit.itp@gmail.com'),
(38, 38, 'pulkit.itp@gmail.com'),
(39, 39, 'pulkit.itp@gmail.com'),
(40, 40, 'pulkit.itp@gmail.com'),
(41, 41, 'pulkit.itp@gmail.com'),
(42, 42, 'pulkit.itp@gmail.com'),
(43, 43, 'pulkit.itp@gmail.com'),
(44, 44, 'pulkit.itp@gmail.com'),
(45, 45, 'pulkit.itp@gmail.com'),
(46, 46, 'pulkit.itp@gmail.com'),
(47, 47, 'pulkit.itp@gmail.com'),
(48, 48, 'pulkit.itp@gmail.com'),
(49, 49, 'pulkit.itp@gmail.com'),
(50, 50, 'pulkit.itp@gmail.com'),
(51, 51, 'pulkit.itp@gmail.com'),
(52, 52, 'pulkit.itp@gmail.com'),
(53, 53, 'pulkit.itp@gmail.com'),
(54, 53, 'pulkit.rnd@gmail.com'),
(55, 54, 'pulkit.itp@gmail.com'),
(56, 54, 'pulkit.rnd@gmail.com'),
(57, 55, 'pulkit.itp@gmail.com'),
(58, 56, 'pulkit.itp@gmail.com'),
(59, 57, 'pulkit.itp@gmail.com'),
(60, 58, 'pulkit.itp@gmail.com'),
(61, 59, 'pulkit.itp@gmail.com'),
(62, 60, 'pulkit.itp@gmail.com'),
(63, 61, 'pulkit.itp@gmail.com'),
(64, 62, 'pulkit.itp@gmail.com'),
(65, 63, 'pulkit.itp@gmail.com'),
(66, 64, 'pulkit.itp@gmail.com'),
(67, 65, 'pulkit.itp@gmail.com'),
(68, 66, 'pulkit.itp@gmail.com'),
(69, 67, 'pulkit.itp@gmail.com'),
(70, 68, 'pulkit.itp@gmail.com'),
(71, 69, 'pulkit.itp@gmail.com'),
(72, 70, 'pulkit.itp@gmail.com'),
(73, 71, 'pulkit.itp@gmail.com'),
(74, 72, 'pulkit.itp@gmail.com'),
(75, 73, 'pulkit.itp@gmail.com'),
(76, 74, 'pulkit.itp@gmail.com'),
(77, 75, 'pulkit.itp@gmail.com'),
(78, 76, 'pulkit.itp@gmail.com'),
(79, 77, 'pulkit.itp@gmail.com'),
(80, 78, 'pulkit.itp@gmail.com'),
(81, 79, 'pulkit.itp@gmail.com'),
(82, 80, 'pulkit.itp@gmail.com'),
(83, 81, 'pulkit.itp@gmail.com'),
(84, 82, 'pulkit.itp@gmail.com'),
(85, 83, 'pulkit.itp@gmail.com'),
(86, 84, 'pulkit.itp@gmail.com'),
(87, 84, 'pulkit.rnd@gmail.com'),
(88, 85, 'pulkit.itp@gmail.com'),
(89, 85, 'pulkit.rnd@gmail.com'),
(90, 86, 'pulkit.itp@gmail.com'),
(91, 86, 'pulkit.rnd@gmail.com'),
(92, 87, 'pulkit.itp@gmail.com'),
(93, 87, 'pulkit.rnd@gmail.com'),
(94, 88, 'pulkit.itp@gmail.com'),
(95, 88, 'pulkit.rnd@gmail.com'),
(96, 89, 'pulkit.itp@gmail.com'),
(97, 89, 'pulkit.rnd@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` text,
  `lastname` text,
  `street` text,
  `address2` text,
  `city` text,
  `state` text,
  `postal` text,
  `country` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `passwordreset` text NOT NULL,
  `password` text NOT NULL,
  `timestamp` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `street`, `address2`, `city`, `state`, `postal`, `country`, `email`, `mobile`, `active`, `passwordreset`, `password`, `timestamp`) VALUES
(3, 'Pulkit', 'Gupta', '', 'Sri nagar rani bagh1', 'Delhi', 'Delhi', '110034', 'Canada', 'pulkit.itp@gmail.com', '919958140688', 1, '219c00481c2150a09a750eaecc15ac6c', '96ac59657aecb0e65afb4aaa0bdb7cfc', '1355002773002'),
(4, 'Ankit', 'Gupta', 'Wz-624 Ivrd Floor', 'Sri nagar Pitampura', 'Delhi', 'Delhi', '110034', 'United Kingdom', 'pulkit.rnd@gmail.com', '9210061566', 1, '13fdf413915da55bf4009d60f87b87d2', '4873e54f1b9a84f05dc858de98fbb961', '1350686654163'),
(5, 'ankur', 'sethi', 'wwsss', 'khb', 'khjb', 'khb', 'kjb', 'United Kingdom', 'ankur@mailinator.com', '', 1, '9703c6e58b65106aa00e751584c8c4f3', 'b8bb32a953f0d051f8df58ecbce17caf', '1350847566125'),
(6, 'Pulkit', 'g', 'gg', 'gg', 'g', 'gg', 'g', '', 'pulki@malinator.com', '', 0, '119fe462722c8d05f9f8bf5d796f43a5', 'a33a7dca3415af291a108717727e11ce', '1350909260961'),
(7, 'Pulkit', 'g', 'gg', 'gg', 'g', 'gg', 'g', '', 'pulkit@malinator.com', '', 0, 'c0b6eb2f7fa48de3401a986a804890a4', '3f86805dbe055fc08893b60c9e78df65', '1350909291170'),
(8, 'pp', 'pp', 'pp', 'pp', 'pp', 'pp', 'pp', '', 'apulkit@malinator.com', '', 0, '45155aef085580505f31a4ff827d6d90', 'a42c7b51e4c0f3ea12cb97334a33818e', '1350909772737'),
(9, 'pp', 'pp', 'pp', 'pp', 'pp', 'pp', 'pp', '', 'awpulkit@malinator.com', '', 0, '778b3cf5b0b2a373ea4d1daa949bdb52', 'e1889562e2b0e5fc822433f21a2badfa', '1350910014525'),
(10, 'Pulkit', 'p', 'p', 'p', 'p', 'p', 'p', '', 'pulkitg@malinator.com', 'p', 0, '4277e7dba4a61225f53500927088e3f1', '237a9a38d215702d044faa71f71f6b8b', '1351607673210'),
(11, 'p', 'p', 'p', 'p', 'p', 'p', 'p', '', 'p@p.com', 'p', 0, '1b93a10a12e060b8146b820e4e279cdb', 'f56d6b77ce8016aa95645f6ba3ca97ba', '1351698398766'),
(12, 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'India', 'ppp@mailinator.com', '', 1, '8cc5df890ff0a440927f40378d43285c', '4f432c2092284bcc3ae4ef16dea2eb23', '1351947147177'),
(13, 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'India', 'jj@mailinator.com', '', 1, 'b06680968b4ac0ed1a9fde4a97140cf8', 'ea01bf24118cee6287140c183423843c', '1351947241434'),
(14, 'p', 'p', 'p', 'p', 'p', 'p', 'p', '', 'p@mailinator.com', '', 0, 'a4125680746924adf23bc65b5b870f64', 'c71219b5b86359d7b04cc7f1a0000d9c', '1351947605699'),
(15, 'p', 'p', 'p', 'p', 'p', 'p', 'p', '', 'abc@mailinator.com', '', 1, '550256aaed8726e7c912cded7e7a96b8', '2ab35afc2c8b9fef715dd011bcb01310', '1351947822940'),
(16, 'P', 'p', '', '', '', '', '', '', 'kk@mailinator.com', '', 0, '7532a19f20e249c07db77d5959c137b6', '8947cdd692d3c1bfe4088fd8bbc8adc7', '1352132100772'),
(17, 'p', 'pp', '', '', '', '', '', '', 'p1@mailinator.com', '44', 1, '6230bb38f6e0eb41327a2f34033cd089', '88736333412be90f801448a9f8ab6607', '1352132213196'),
(18, 'pp', 'pp', '', '', '', '', '', '', 'aap@mailinator.com', '', 0, '7d9914bfb9b6a22eabc4f9c8ecb759aa', '2f2217e7ace273e6ba83cf14740bc789', '1352314735430'),
(19, 'p', 'p', '', '', '', '', '', '', 'pulkit@mailinator.com', '', 0, 'a0b373bb53fe58366dde6787c25a64f4', '45e84e7730e16baa619c8eccb1829c37', '1352410595628'),
(20, 'pl', 'pl', '', '', '', '', '', '', 'pk@mailinator.com', '9958140688', 1, '975a561a118782009512fd1fcd3c95a5', 'faac5ee5350ad4762916d75afda07c07', '1352563865649');
